#import <QuartzCore/QuartzCore.h>
@interface CABasicAnimation (JPImageresizer)
+ (CABasicAnimation *)jpir_backwardsAnimationWithKeyPath:(NSString *)keyPath
                                               fromValue:(id)fromValue
                                                 toValue:(id)toValue
                                      timingFunctionName:(CAMediaTimingFunctionName)timingFunctionName
                                                duration:(NSTimeInterval)duration;
@end
@interface CALayer (JPImageresizer)
- (void)jpir_addBackwardsAnimationWithKeyPath:(NSString *)keyPath
                                    fromValue:(id)fromValue
                                      toValue:(id)toValue
                           timingFunctionName:(CAMediaTimingFunctionName)timingFunctionName
                                     duration:(NSTimeInterval)duration;
@end
