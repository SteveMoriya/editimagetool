#pragma mark - 枚举
typedef NS_ENUM(NSUInteger, JPImageresizerFrameType) {
    JPConciseFrameType, 
    JPClassicFrameType
};
typedef NS_ENUM(NSUInteger, JPAnimationCurve) {
    JPAnimationCurveEaseInOut, 
    JPAnimationCurveEaseIn,
    JPAnimationCurveEaseOut,
    JPAnimationCurveLinear
};
typedef NS_ENUM(NSUInteger, JPImageresizerRotationDirection) {
    JPImageresizerVerticalUpDirection = 0,  
    JPImageresizerHorizontalLeftDirection,
    JPImageresizerVerticalDownDirection,
    JPImageresizerHorizontalRightDirection
};
#pragma mark - Block
typedef void(^JPImageresizerIsCanRecoveryBlock)(BOOL isCanRecovery);
typedef void(^JPImageresizerIsPrepareToScaleBlock)(BOOL isPrepareToScale);
