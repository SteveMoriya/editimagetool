#import <UIKit/UIKit.h>
#import "JPImageresizerConfigure.h"
#import "JPImageresizerFrameView.h"
@interface JPImageresizerView : UIView
@property (nonatomic, weak, readonly) UIScrollView *scrollView;
@property (nonatomic, weak, readonly) UIImageView *imageView;
@property (nonatomic, weak, readonly) JPImageresizerFrameView *frameView;
+ (instancetype)imageresizerViewWithConfigure:(JPImageresizerConfigure *)configure
                    imageresizerIsCanRecovery:(JPImageresizerIsCanRecoveryBlock)imageresizerIsCanRecovery
                 imageresizerIsPrepareToScale:(JPImageresizerIsPrepareToScaleBlock)imageresizerIsPrepareToScale;
- (instancetype)initWithResizeImage:(UIImage *)resizeImage
                              frame:(CGRect)frame
                          frameType:(JPImageresizerFrameType)frameType
                     animationCurve:(JPAnimationCurve)animationCurve
                         blurEffect:(UIBlurEffect *)blurEffect
                            bgColor:(UIColor *)bgColor
                          maskAlpha:(CGFloat)maskAlpha
                        strokeColor:(UIColor *)strokeColor
                      resizeWHScale:(CGFloat)resizeWHScale
                      contentInsets:(UIEdgeInsets)contentInsets
                        borderImage:(UIImage *)borderImage
               borderImageRectInset:(CGPoint)borderImageRectInset
                   maximumZoomScale:(CGFloat)maximumZoomScale
                      isRoundResize:(BOOL)isRoundResize
                      isShowMidDots:(BOOL)isShowMidDots
          imageresizerIsCanRecovery:(JPImageresizerIsCanRecoveryBlock)imageresizerIsCanRecovery
       imageresizerIsPrepareToScale:(JPImageresizerIsPrepareToScaleBlock)imageresizerIsPrepareToScale;
@property (nonatomic) JPImageresizerFrameType frameType;
@property (nonatomic, assign) JPAnimationCurve animationCurve;
@property (readonly) CGSize baseContentMaxSize;
@property (nonatomic) UIImage *resizeImage;
@property (nonatomic) UIBlurEffect *blurEffect;
@property (nonatomic) UIColor *bgColor;
@property (nonatomic) CGFloat maskAlpha;
@property (nonatomic) UIColor *strokeColor;
@property (nonatomic) CGFloat resizeWHScale;
@property (nonatomic) CGFloat initialResizeWHScale;
@property (readonly) CGFloat imageresizeWHScale;
@property (nonatomic, assign) BOOL edgeLineIsEnabled;
@property (nonatomic, assign) BOOL isClockwiseRotation;
@property (nonatomic) BOOL isLockResizeFrame;
@property (nonatomic, assign) BOOL verticalityMirror;
@property (nonatomic, assign) BOOL horizontalMirror;
@property (nonatomic, assign) BOOL isPreview;
@property (nonatomic) UIImage *borderImage;
@property (nonatomic) CGPoint borderImageRectInset;
@property (nonatomic) BOOL isShowMidDots;
- (void)setResizeImage:(UIImage *)resizeImage animated:(BOOL)isAnimated transition:(UIViewAnimationTransition)transition;
- (void)setupStrokeColor:(UIColor *)strokeColor
              blurEffect:(UIBlurEffect *)blurEffect
                 bgColor:(UIColor *)bgColor
               maskAlpha:(CGFloat)maskAlpha
                animated:(BOOL)isAnimated;
- (void)setResizeWHScale:(CGFloat)resizeWHScale isToBeArbitrarily:(BOOL)isToBeArbitrarily animated:(BOOL)isAnimated;
- (void)roundResize:(BOOL)isAnimated;
- (BOOL)isRoundResizing;
- (void)setVerticalityMirror:(BOOL)verticalityMirror animated:(BOOL)isAnimated;
- (void)setHorizontalMirror:(BOOL)horizontalMirror animated:(BOOL)isAnimated;
- (void)setIsPreview:(BOOL)isPreview animated:(BOOL)isAnimated;
- (void)updateResizeImage:(UIImage *)resizeImage;
- (void)rotation;
- (void)recoveryByCurrentResizeWHScale;
- (void)recoveryToRoundResize;
- (void)recoveryByInitialResizeWHScale:(BOOL)isToBeArbitrarily;
- (void)recoveryByTargetResizeWHScale:(CGFloat)targetResizeWHScale isToBeArbitrarily:(BOOL)isToBeArbitrarily;
- (void)originImageresizerWithComplete:(void(^)(UIImage *resizeImage))complete;
- (void)imageresizerWithComplete:(void(^)(UIImage *resizeImage))complete compressScale:(CGFloat)compressScale;
- (void)updateFrame:(CGRect)frame contentInsets:(UIEdgeInsets)contentInsets duration:(NSTimeInterval)duration;
@end
