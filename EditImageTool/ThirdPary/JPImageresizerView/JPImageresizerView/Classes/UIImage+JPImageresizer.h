#import <UIKit/UIKit.h>
@interface UIImage (JPImageresizer)
+ (UIImage *)jpir_resultImageWithImage:(UIImage *)originImage
                             cropFrame:(CGRect)cropFrame
                          relativeSize:(CGSize)relativeSize
                           isVerMirror:(BOOL)isVerMirror
                           isHorMirror:(BOOL)isHorMirror
                     rotateOrientation:(UIImageOrientation)orientation
                           isRoundClip:(BOOL)isRoundClip
                         compressScale:(CGFloat)scale;
@end
