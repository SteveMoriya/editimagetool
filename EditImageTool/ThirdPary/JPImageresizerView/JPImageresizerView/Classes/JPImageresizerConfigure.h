#import <UIKit/UIKit.h>
#import "JPImageresizerTypedef.h"
@interface JPImageresizerConfigure : NSObject
+ (instancetype)defaultConfigureWithResizeImage:(UIImage *)resizeImage make:(void(^)(JPImageresizerConfigure *configure))make;
+ (instancetype)lightBlurMaskTypeConfigureWithResizeImage:(UIImage *)resizeImage make:(void (^)(JPImageresizerConfigure *configure))make;
+ (instancetype)darkBlurMaskTypeConfigureWithResizeImage:(UIImage *)resizeImage make:(void (^)(JPImageresizerConfigure *configure))make;
@property (nonatomic, strong) UIImage *resizeImage;
@property (nonatomic, assign) CGRect viewFrame;
@property (nonatomic, assign) JPImageresizerFrameType frameType;
@property (nonatomic, assign) JPAnimationCurve animationCurve;
@property (nonatomic, strong) UIBlurEffect *blurEffect;
@property (nonatomic, strong) UIColor *bgColor;
@property (nonatomic, assign) CGFloat maskAlpha;
@property (nonatomic, strong) UIColor *strokeColor;
@property (nonatomic, assign) CGFloat resizeWHScale;
@property (nonatomic, assign) BOOL edgeLineIsEnabled;
@property (nonatomic, assign) UIEdgeInsets contentInsets;
@property (nonatomic, assign) BOOL isClockwiseRotation;
@property (nonatomic, strong) UIImage *borderImage;
@property (nonatomic, assign) CGPoint borderImageRectInset;
@property (nonatomic, assign) CGFloat maximumZoomScale;
@property (nonatomic, assign) BOOL isRoundResize;
@property (nonatomic, assign) BOOL isShowMidDots;
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_resizeImage)(UIImage *resizeImage);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_viewFrame)(CGRect viewFrame);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_frameType)(JPImageresizerFrameType frameType);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_animationCurve)(JPAnimationCurve animationCurve);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_blurEffect)(UIBlurEffect *blurEffect);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_bgColor)(UIColor *bgColor);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_maskAlpha)(CGFloat maskAlpha);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_strokeColor)(UIColor *strokeColor);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_resizeWHScale)(CGFloat resizeWHScale);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_edgeLineIsEnabled)(BOOL edgeLineIsEnabled);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_contentInsets)(UIEdgeInsets contentInsets);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_isClockwiseRotation)(BOOL isClockwiseRotation);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_borderImage)(UIImage *borderImage);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_borderImageRectInset)(CGPoint borderImageRectInset);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_maximumZoomScale)(CGFloat maximumZoomScale);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_isRoundResize)(BOOL isRoundResize);
@property (nonatomic, readonly) JPImageresizerConfigure *(^jp_isShowMidDots)(BOOL isShowMidDots);
@end
