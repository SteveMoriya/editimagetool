//
//  CDHUBView.m
//  EditImageTool
//
//  Created by steve on 2020/8/24.
//  Copyright © 2020 India. All rights reserved.
//

#import "CDHUBView.h"

@interface CDHUBView()

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation CDHUBView

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews:frame];
    }
    return self;
}

- (void)initSubviews:(CGRect) frame {
    self.frame = frame;
    self.userInteractionEnabled = YES;
    
    [self addSubview:self.indicatorView];
    
}

- (void)show {
    [[[[[[UIApplication sharedApplication] delegate] window] rootViewController] view] addSubview:self];
    //使用window，可以解决隐藏tabbar的问题
//    [[VDDataTool getCurrentVC].navigationController.view addSubview:self];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [ RGB(50, 50, 50) colorWithAlphaComponent:0.3f];
    }];
}

- (void)dismiss {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.3 animations:^{
            self.backgroundColor = [ RGB(50, 50, 50) colorWithAlphaComponent:0.0f];
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    });
}

- (UIActivityIndicatorView *)indicatorView {
     if (!_indicatorView) {
         _indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake((SCREEN_Width - ZoomSize(20))/2, (SCREEN_Height - ZoomSize(20))/2, ZoomSize(20), ZoomSize(20))];
         _indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
         [_indicatorView startAnimating];
     }
    return _indicatorView;
}

@end
