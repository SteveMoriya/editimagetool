//
//  CDHUBView.h
//  EditImageTool
//
//  Created by steve on 2020/8/24.
//  Copyright © 2020 India. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CDHUBView : UIView

- (instancetype) initWithFrame:(CGRect)frame;

- (void)show;

- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
