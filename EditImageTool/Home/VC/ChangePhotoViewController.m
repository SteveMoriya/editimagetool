//
//  ChangePhotoViewController.m
//  EditImageTool
//
//  Created by steve on 2020/8/18.
//  Copyright © 2020 India. All rights reserved.
//

#import "ChangePhotoViewController.h"
#import "VDChoosePhotoTool.h"
#import "AFNetworking.h"
#import <Photos/Photos.h>

@interface ChangePhotoViewController (){
    UIImageView *getImageView ;
    NSData *getImageData;
    
    NSString *tokenString;
    AFHTTPSessionManager *manager;
    
    UIScrollView *btScrollView;
    NSArray *styleArray;
    NSArray *vipIndexArray;
    
    int chooseIndex; //上一次选择的按钮，防止连点
}

@property (nonatomic, strong) VDChoosePhotoTool *photoManager;

@end

@implementation ChangePhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    styleArray = [NSArray arrayWithObjects:@"cartoon",@"pencil",@"color_pencil",@"warm",@"wave",@"lavender",@"mononoke",@"scream",@"gothic", nil];
    vipIndexArray = [NSArray arrayWithObjects:@"2", @"3", @"4", @"5", @"7", @"8", @"9", nil];
    
    [self setPage];
    [self setData];
}

- (void) setPage {
    
    self.view.backgroundColor = UIColor.blackColor;
    
    UIButton * closeBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(20), StatusBarHeight, ZoomSize(30), ZoomSize(30))];
    [self.view addSubview:closeBt];
    [closeBt setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [closeBt addTarget:self action:@selector(closeAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton * reloadBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(150), StatusBarHeight, ZoomSize(30), ZoomSize(30))];
    [self.view addSubview:reloadBt];
    [reloadBt setBackgroundImage:[UIImage imageNamed:@"reflesh"] forState:UIControlStateNormal];
    [reloadBt addTarget:self action:@selector(chooseAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton * saveBt = [[UIButton alloc] initWithFrame:CGRectMake( SCREEN_Width - ZoomSize(100), StatusBarHeight, ZoomSize(30), ZoomSize(30))];
    [self.view addSubview:saveBt];
    [saveBt setBackgroundImage:[UIImage imageNamed:@"save"] forState:UIControlStateNormal];
    [saveBt addTarget:self action:@selector(saveData) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton * shareBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width - ZoomSize(50), StatusBarHeight, ZoomSize(30), ZoomSize(30))];
    [self.view addSubview:shareBt];
    [shareBt setBackgroundImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
    [shareBt addTarget:self action:@selector(shareData) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, NavigationHeight, SCREEN_Width, ZoomSize(1))];
    [self.view addSubview:lineView];
    lineView.backgroundColor = RGB(200, 200, 200);
    
    getImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, NavigationHeight + ZoomSize(10), SCREEN_Width, SCREEN_Height - NavigationHeight - BottomSafeAreaHeight - ZoomSize(180))];
    getImageView.backgroundColor = UIColor.blackColor;
    getImageView.contentMode = UIViewContentModeScaleAspectFill;
    getImageView.layer.masksToBounds = YES;
    [self.view addSubview:getImageView];
    
    
    UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(getImageView.frame) + ZoomSize(10), SCREEN_Width, ZoomSize(20))];
    [self.view addSubview:tipLb];
    tipLb.text = @"Please show your face";
    tipLb.textColor = RGB(200, 200, 200);
    tipLb.textAlignment = NSTextAlignmentCenter;
    
    UIView *tipLineView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(getImageView.frame) + ZoomSize(40), SCREEN_Width, ZoomSize(1))];
    [self.view addSubview:tipLineView];
    tipLineView.backgroundColor = RGB(200, 200, 200);
    
    
    UILabel *typeTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(20), CGRectGetMaxY(tipLineView.frame) + ZoomSize(10), ZoomSize(200), ZoomSize(20))];
    [self.view addSubview:typeTipLb];
    typeTipLb.text = @"Cartoon Style";
    typeTipLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
    typeTipLb.textColor = RGB(200, 200, 200);
    
    btScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(typeTipLb.frame) + ZoomSize(10), SCREEN_Width, ZoomSize(80))];
    [self.view addSubview:btScrollView];
    
    btScrollView.contentSize = CGSizeMake(ZoomSize(70) * styleArray.count + ZoomSize(80), ZoomSize(80));
    
    for (int i = 0; i <= styleArray.count; i++) {
        
        UIButton * exchangeBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10) +  ZoomSize(70) * i , ZoomSize(0), ZoomSize(60), ZoomSize(80))];
        exchangeBt.tag = 100 + i;
        [btScrollView addSubview:exchangeBt];
        [exchangeBt setImage:[UIImage imageNamed:[NSString stringWithFormat:@"masker_%d",i]] forState:UIControlStateNormal];
        [exchangeBt addTarget:self action:@selector(exchangeAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        if ([vipIndexArray containsObject:[NSString stringWithFormat:@"%d", i]]) {
            
            UILabel *vipTipLb = [[UILabel alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(0), ZoomSize(40), ZoomSize(20))];
            vipTipLb.backgroundColor = UIColor.redColor;
            vipTipLb.text = @"VIP";
            vipTipLb.textAlignment = NSTextAlignmentCenter;
            vipTipLb.textColor = UIColor.whiteColor;
            vipTipLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
            [exchangeBt addSubview:vipTipLb];
            
        }
    }
    
}

- (void) setData {
    
    getImageView.image = self.getImg;
    self->getImageData = UIImageJPEGRepresentation([VDDataTool compressSizeImage:self.getImg], 0.25);
    
    _photoManager = [[VDChoosePhotoTool alloc] init];
    
    manager = [[AFHTTPSessionManager alloc] init];
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //request 格式
    manager.responseSerializer = [AFJSONResponseSerializer serializer]; //response 格式
    manager.requestSerializer.timeoutInterval = 30.f;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html",nil];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    //    获取token
    NSDictionary *dic = [[NSDictionary alloc] init];
    [manager POST:TokenUrl parameters:dic headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dataDic = responseObject;
        self->tokenString= dataDic[@"access_token"];
        
        [self directSetPage];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    }];
    
}

#pragma function

- (void) directSetPage {
    
    NSString *imageStr = [getImageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    if (imageStr.length > 0) {
        
        NSDictionary *dic = @{@"image":imageStr, @"option":@"cartoon"};
        
        [VDDataTool startShowHUD];
        [manager POST:[NSString stringWithFormat:@"%@%@", ImgUrl, tokenString] parameters:dic headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [VDDataTool dismissHUD];
            
            NSDictionary *dataDic = responseObject;
            NSString *getImgStr = dataDic[@"image"];
            
            if (getImgStr.length > 0) {
                
                NSData * imageData =[[NSData alloc] initWithBase64EncodedString:getImgStr options:NSDataBase64DecodingIgnoreUnknownCharacters];
                UIImage *image = [UIImage imageWithData: imageData];
                
                self->getImageView.image = image;
                self->chooseIndex = 1;
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [VDDataTool dismissHUD];
        }];
        
    }
    
}

- (void) closeAction {
    NSLog(@"closeAction");
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) chooseAction  {
    NSLog(@"choose Action");
    
    [_photoManager startSelectPhotoWithImageName:@"Choose Photo"];
    __weak __typeof (self) weakSelf = self;
    
    _photoManager.successHandle = ^(VDChoosePhotoTool * _Nonnull manager, UIImage * _Nonnull originalImage, UIImage * _Nonnull resizeImage) {
        __strong __typeof(self) strongSelf = weakSelf;
        
        strongSelf->_getImg = originalImage;
        strongSelf->getImageData = UIImageJPEGRepresentation([VDDataTool compressSizeImage:self.getImg], 0.25);
        strongSelf->getImageView.image = originalImage;
        
        [strongSelf directSetPage];
    };
    
}

- (void) saveData {
    NSLog(@"saveData");
    
    [[PHPhotoLibrary sharedPhotoLibrary]performChanges:^{
        [PHAssetChangeRequest creationRequestForAssetFromImage:self->getImageView.image];
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
        if (error) {
            [VDDataTool showHUDWithString:@"Save failed!"];
        } else {
            [VDDataTool showHUDWithString:@"Save success!"];
        }
    }];
    
}

- (void) shareData {
    NSLog(@"shareData");
    
    NSString *shareTitle = @"Wow! Did you check my cartoon selfie? Have a look on Cado.";
    UIImage *shareImage = [UIImage imageNamed:@"shareImg"];
    NSURL *shareUrl = [NSURL URLWithString:@"https://apps.apple.com/us/app/id1528329290"];
    NSArray *activityItemsArray = @[shareTitle, shareImage, shareUrl]; // 分享内容
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItemsArray applicationActivities:nil];
    activityVC.modalInPopover = YES;
    
    activityVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
    };
    
    [self.navigationController presentViewController:activityVC animated:YES completion:nil];
    
}


//image    true    string    -    base64编码后大小不超过4M，像素乘积不超过2000*2000，最短边至少50px，最长边最大4096px，长宽比3:1以内。注意：图片的base64编码是不包含图片头的，如（data:image/jpg;base64,）

//gothic    cartoon：卡通画风格
//pencil：铅笔风格
//color_pencil：彩色铅笔画风格
//warm：彩色糖块油画风格
//wave：神奈川冲浪里油画风格
//lavender：薰衣草油画风格
//mononoke：奇异油画风格
//scream：呐喊油画风格
//gothic：哥特油画风格


- (void) exchangeAction :(UIButton *)sender {
    
    int senderTag = (int)sender.tag - 100;
    
    //判断vip
    if ([vipIndexArray containsObject:[NSString stringWithFormat:@"%d", senderTag]]) {
        if (![[CadoUserInfo shareInstant] checkUserVip]) {
            [VDDataTool showBuyVipPage];
            return;
        }
    }
    
    NSString *imageStr = [getImageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSDictionary *dic = [[NSDictionary alloc] init];
    
    if (senderTag == 0) {
        getImageView.image = self.getImg;
        return;
    } else {
        if ( chooseIndex == senderTag ) {
            return; //防止连点
        }
        chooseIndex = senderTag;
        NSString *styleString = styleArray[senderTag - 1];
        dic = @{@"image":imageStr, @"option":styleString};
    }
    
    if (imageStr.length > 0) {
        
        [VDDataTool startShowHUD];
        [manager POST:[NSString stringWithFormat:@"%@%@", ImgUrl, tokenString] parameters:dic headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [VDDataTool dismissHUD];
            
            NSDictionary *dataDic = responseObject;
            NSString *getImgStr = dataDic[@"image"];
            if (getImgStr.length > 0) {
                NSData * imageData =[[NSData alloc] initWithBase64EncodedString:getImgStr options:NSDataBase64DecodingIgnoreUnknownCharacters];
                UIImage *image = [UIImage imageWithData: imageData];
                
                self->getImageView.image = image;
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [VDDataTool dismissHUD];
        }];
    }
}


@end
