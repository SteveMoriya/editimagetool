//
//  HomeViewController.m
//  EditImageTool
//
//  Created by steve on 2020/8/18.
//  Copyright © 2020 India. All rights reserved.
//

#import "HomeViewController.h"
#import "SettingViewController.h"
#import "VDChoosePhotoTool.h"
//#import "SDWebImageManager.h"
#import <AVFoundation/AVFoundation.h>
#import "ChangePhotoViewController.h"

@interface HomeViewController ()

@property (nonatomic, strong) VDChoosePhotoTool *photoManager;
@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) AVPlayerLayer *playerLayer;
@property (nonatomic, strong) AVPlayer *player;


@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setPage];
    [self setData];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.player play];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self.player pause];
}

- (void) setPage {
    
    self.view.backgroundColor = UIColor.blackColor;
    
    UIButton * settingBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(20), StatusBarHeight, ZoomSize(30), ZoomSize(30))];
    [self.view addSubview:settingBt];
    [settingBt setBackgroundImage:[UIImage imageNamed:@"Settings"] forState:UIControlStateNormal];
    [settingBt addTarget:self action:@selector(settingAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton * feedbackBt = [[UIButton alloc] initWithFrame:CGRectMake( SCREEN_Width - ZoomSize(100), StatusBarHeight, ZoomSize(80), ZoomSize(30))];
    [self.view addSubview:feedbackBt];
    [feedbackBt setTitle:@"Feedback" forState:UIControlStateNormal];
    [feedbackBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    [feedbackBt addTarget:self action:@selector(feedbackAction) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"effect" ofType:@"mov"];// 1、获取媒体资源地址
    NSURL *sourceMovieURL = [NSURL fileURLWithPath:path];
    AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:sourceMovieURL]; // 2、创建AVPlayerItem
    self.player = [AVPlayer playerWithPlayerItem:playerItem]; // 3、根据AVPlayerItem创建媒体播放器
    AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player]; // 4、创建AVPlayerLayer，用于呈现视频
    playerLayer.frame = CGRectMake(0, NavigationHeight, SCREEN_Width, SCREEN_Height - NavigationHeight);// 5、设置显示大小和位置
    playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:playerLayer];
    
    UIButton * photoBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width/4 - ZoomSize(30), SCREEN_Height - ZoomSize(120) - BottomSafeAreaHeight, ZoomSize(60), ZoomSize(60))];
    [self.view addSubview:photoBt];
    [photoBt setBackgroundImage:[UIImage imageNamed:@"album"] forState:UIControlStateNormal];
    [photoBt addTarget:self action:@selector(photoAction) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton * cameraBt = [[UIButton alloc] initWithFrame:CGRectMake( SCREEN_Width/4 *3 - ZoomSize(30) , SCREEN_Height - ZoomSize(120) - BottomSafeAreaHeight , ZoomSize(60), ZoomSize(60))];
    [self.view addSubview:cameraBt];
    [cameraBt setBackgroundImage:[UIImage imageNamed:@"camera"] forState:UIControlStateNormal];
    [cameraBt addTarget:self action:@selector(cameraAction) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void) setData {
    
    _photoManager = [[VDChoosePhotoTool alloc] init];
    
    //     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil]; //监听播放进度，用于设置循环播放
}

- (void)moviePlayDidEnd:(NSNotification*)notification{
    AVPlayerItem*item = [notification object];
    [item seekToTime:kCMTimeZero];
    [self.player play];
}

#pragma function

- (void) settingAction {
    NSLog(@"settingAction");
    
    SettingViewController *vc = [[SettingViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) feedbackAction {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto://lixkauditer@gmail.com"]];
}

- (void) photoAction {
    NSLog(@"photoAction");
    
    [_photoManager chooseLibrary];
    
    __weak __typeof (self) weakSelf = self;
    _photoManager.successHandle = ^(VDChoosePhotoTool * _Nonnull manager, UIImage * _Nonnull originalImage, UIImage * _Nonnull resizeImage) {
        __strong __typeof(self) strongSelf = weakSelf;
        
        ChangePhotoViewController *vc = [[ChangePhotoViewController alloc] init];
        vc.getImg = [VDDataTool compressSizeImage:originalImage];
        [strongSelf.navigationController pushViewController:vc animated:YES];
        //        [strongSelf.navigationController presentViewController:vc animated:YES completion:nil];

    };
    
}

- (void) cameraAction {
    NSLog(@"cameraAction");
    
    [_photoManager openPhoto];
    
    __weak __typeof (self) weakSelf = self;
    _photoManager.successHandle = ^(VDChoosePhotoTool * _Nonnull manager, UIImage * _Nonnull originalImage, UIImage * _Nonnull resizeImage) {
        __strong __typeof(self) strongSelf = weakSelf;
        
        ChangePhotoViewController *vc = [[ChangePhotoViewController alloc] init];
        vc.getImg = [VDDataTool compressSizeImage:resizeImage];
        [strongSelf.navigationController pushViewController:vc animated:YES];
    };
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [_photoManager startSelectPhotoWithImageName:@"Choose Photo"];
    __weak __typeof (self) weakSelf = self;
    
    _photoManager.successHandle = ^(VDChoosePhotoTool * _Nonnull manager, UIImage * _Nonnull originalImage, UIImage * _Nonnull resizeImage) {
        __strong __typeof(self) strongSelf = weakSelf;
        
        ChangePhotoViewController *vc = [[ChangePhotoViewController alloc] init];
        vc.getImg = [VDDataTool compressSizeImage:resizeImage];
        [strongSelf.navigationController pushViewController:vc animated:YES];
    };
    
    
}

@end
