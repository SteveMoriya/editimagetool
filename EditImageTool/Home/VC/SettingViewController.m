//
//  SettingViewController.m
//  EditImageTool
//
//  Created by steve on 2020/8/18.
//  Copyright © 2020 India. All rights reserved.
//

#import "SettingViewController.h"
#import "VDWebVC.h"
#import "VDPurchaseTool.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setPage];
    [self setData];
}

- (void) setPage {
    
    self.view.backgroundColor = UIColor.blackColor;
    
    UIButton * backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(20), StatusBarHeight, ZoomSize(30), ZoomSize(30))];
    [self.view addSubview:backBt];
    [backBt setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake( SCREEN_Width/2 - ZoomSize(60), StatusBarHeight, ZoomSize(120), ZoomSize(30))];
    [self.view addSubview:titleLb];
    titleLb.textColor = WhiteColor;
    titleLb.adjustsFontSizeToFitWidth = YES;
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
    titleLb.text = @"Setting";
    
    
    UIView *tipLineView = [[UIView alloc] initWithFrame:CGRectMake(0, NavigationHeight +  ZoomSize(20), SCREEN_Width, ZoomSize(1))];
    [self.view addSubview:tipLineView];
    tipLineView.backgroundColor = RGB(200, 200, 200);
    
    
#pragma FeedbackBt
    UIButton *feedBackBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(0), NavigationHeight +  ZoomSize(20), SCREEN_Width , ZoomSize(48)) btEnabled:YES titleString:@"Feedback" tipString:nil imageString:@"in" btnTag:110];
    [self.view addSubview:feedBackBt];
    
#pragma privacyPolicyBt
    UIButton *privacyPolicyBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(0), NavigationHeight +  ZoomSize(70), SCREEN_Width , ZoomSize(48)) btEnabled:YES titleString:@"Privacy Policy" tipString:nil imageString:@"in" btnTag:120];
    [self.view addSubview:privacyPolicyBt];
    
#pragma termOfUseBt
    UIButton *termOfUseBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(0), NavigationHeight +  ZoomSize(120), SCREEN_Width , ZoomSize(48)) btEnabled:YES titleString:@"Term Of Use" tipString:nil imageString:@"in" btnTag:130];
    [self.view addSubview:termOfUseBt];
    
#pragma restoreBt
    UIButton *restoreBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(0), NavigationHeight +  ZoomSize(170), SCREEN_Width , ZoomSize(48)) btEnabled:YES titleString:@"Restore Purchase" tipString:nil imageString:@"in" btnTag:140];
    [self.view addSubview:restoreBt];
    
    
    UILabel *vipTipLb = [[UILabel alloc] initWithFrame:CGRectMake( ZoomSize(10), NavigationHeight +  ZoomSize(220), SCREEN_Width, ZoomSize(50))];
    [self.view addSubview:vipTipLb];
    vipTipLb.textColor = WhiteColor;
    vipTipLb.font = [UIFont systemFontOfSize:ZoomSize(18)];
    if ([[CadoUserInfo shareInstant] checkUserVip]) {
        titleLb.text = @"You're VIP";
    }
}

- (void) setData {
    
}

- (UIButton *) createFunctionBtWithFrame:(CGRect) frame btEnabled:(BOOL) btEnabled  titleString:(NSString *) titleString tipString:(NSString *) tipString imageString:(NSString *) imageString btnTag:(int) btnTag  {
    
    UIButton *funcBt = [[UIButton alloc] initWithFrame:frame];
    [funcBt setTitle:titleString forState:UIControlStateNormal];
    [funcBt setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    funcBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
    funcBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    funcBt.titleEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(13), 0, 0);
    funcBt.enabled = btEnabled;
    funcBt.layer.cornerRadius = ZoomSize(16);
    funcBt.layer.masksToBounds = YES;
    funcBt.tag = btnTag;
    [funcBt addTarget:self action:@selector(switchFunctionAction:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *contactImagView = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width - ZoomSize(40), (frame.size.height - ZoomSize(20))/2.0 , ZoomSize(20), ZoomSize(20))];
    contactImagView.image = [UIImage imageNamed:imageString];
    [funcBt addSubview:contactImagView];
    
    
    UIView *tipLineView = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height - ZoomSize(2), SCREEN_Width, ZoomSize(1))];
    [funcBt addSubview:tipLineView];
    tipLineView.backgroundColor = RGB(200, 200, 200);
    
    
    return funcBt;
}

- (void)switchFunctionAction:(UIButton *) sender {
    
    switch (sender.tag) {

        case 110: {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto://lixkauditer@gmail.com"]];
            break;
        }
        case 120: {
            VDWebVC *ActivityVC = [[VDWebVC alloc] init];
            ActivityVC.url = @"https://sites.google.com/view/catoapp/privacy-policy";
            [[VDDataTool getCurrentVC] presentViewController:ActivityVC animated:YES completion:nil];
            break;
        }
        case 130: {
            VDWebVC *ActivityVC = [[VDWebVC alloc] init];
            ActivityVC.url = @"https://sites.google.com/view/catoapp/terms-of-use";
            [[VDDataTool getCurrentVC] presentViewController:ActivityVC animated:YES completion:nil];
            break;
        }
        case 140: {
            [[VDPurchaseTool shareInstance] restorePurchase:YES];
            break;
        }

        default: break;
    }
}

#pragma function

- (void) backAction {
    NSLog(@"settingAction");
    [self.navigationController popViewControllerAnimated:YES];
}



@end
