//
//  UserBuyVipViewController.m
//  EditImageTool
//
//  Created by steve on 2020/8/24.
//  Copyright © 2020 India. All rights reserved.
//

#import "UserBuyVipViewController.h"
#import "VDPurchaseTool.h"
#import "VDWebVC.h"

@interface UserBuyVipViewController ()

@end

@implementation UserBuyVipViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setPage];
    [self setData];
}

- (void) setPage {
    self.view.backgroundColor = UIColor.blackColor;
    
    UIImageView *tipIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, ZoomSize(270))];
    tipIV.image = [UIImage imageNamed:@"vipTip"];
    tipIV.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:tipIV];
    
    UIButton * closeBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(20), StatusBarHeight, ZoomSize(30), ZoomSize(30))];
    [self.view addSubview:closeBt];
    [closeBt setBackgroundImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [closeBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake( SCREEN_Width/2 - ZoomSize(60), ZoomSize(290), ZoomSize(120), ZoomSize(30))];
    [self.view addSubview:titleLb];
    titleLb.textColor = WhiteColor;
    titleLb.adjustsFontSizeToFitWidth = YES;
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
    titleLb.text = @"Cado Vip";
    
    UILabel *tipInfoLb = [[UILabel alloc] initWithFrame:CGRectMake( SCREEN_Width/2 - ZoomSize(80), ZoomSize(330), ZoomSize(160), ZoomSize(20))];
    [self.view addSubview:tipInfoLb];
    tipInfoLb.textColor = UIColor.purpleColor;
    tipInfoLb.adjustsFontSizeToFitWidth = YES;
    tipInfoLb.textAlignment = NSTextAlignmentCenter;
    tipInfoLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    tipInfoLb.text = @"Cartoon your photo";
    
    UIButton *weekBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(20), ZoomSize(360), SCREEN_Width - ZoomSize(40), ZoomSize(50)) titleString:@"WEEKLY" tipString:@"$3.99" btnTag:100];
    [self.view addSubview:weekBt];
    
    UIButton *monthBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(20), ZoomSize(430), SCREEN_Width - ZoomSize(40), ZoomSize(50)) titleString:@"MONTHLY" tipString:@"$12.99" btnTag:110];
    [self.view addSubview:monthBt];
    
    UIButton *yearBt = [self createFunctionBtWithFrame:CGRectMake(ZoomSize(20), ZoomSize(500), SCREEN_Width - ZoomSize(40), ZoomSize(50)) titleString:@"YEARLY" tipString:@"$39.99" btnTag:120];
    [self.view addSubview:yearBt];
    
    
    UIButton *restoreBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(550), SCREEN_Width - ZoomSize(40), ZoomSize(40))];
    [restoreBt setTitle:@"Restore" forState:UIControlStateNormal];
    [restoreBt setTitleColor:WhiteColor forState:UIControlStateNormal];
    [self.view addSubview:restoreBt];
    [restoreBt addTarget:self action:@selector(restoreAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *termsBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width/2 - ZoomSize(80),ZoomSize(580), ZoomSize(80), ZoomSize(40))];
    [termsBt setTitle:@"Terms" forState:UIControlStateNormal];
    [termsBt setTitleColor:UIColor.purpleColor forState:UIControlStateNormal];
    [self.view addSubview:termsBt];
    [termsBt addTarget:self action:@selector(termsAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *privacyBt = [[UIButton alloc] initWithFrame:CGRectMake(SCREEN_Width/2, ZoomSize(580),ZoomSize(80), ZoomSize(40))];
    [privacyBt setTitle:@"Privacy" forState:UIControlStateNormal];
    [privacyBt setTitleColor:UIColor.purpleColor forState:UIControlStateNormal];
    [self.view addSubview:privacyBt];
    [privacyBt addTarget:self action:@selector(privacyAction) forControlEvents:UIControlEventTouchUpInside];
    
    
    
#pragma Terms &Privacy
    NSString *content = @"Payment will be charged to iTunes Account at confirmation of purchase. Subscriptions automatically renew unless auto-renew is turned off at least 24-hours before the end of the current period. Account will be charged for renewal within 24-hours prior to the end of the currentperiod, and identify thecost of the renewal. Subscriptions may be managed by theuser and auto renewal may be turned off by going to the user's Account Settings after purchase. ";
    UITextView *contentTextView = [[UITextView alloc]  initWithFrame:CGRectMake(ZoomSize(16), ZoomSize(620), SCREEN_Width - ZoomSize(32), SCREEN_Height - BottomSafeAreaHeight - ZoomSize(620))];
    [self.view addSubview:contentTextView];
    
    contentTextView.backgroundColor = UIColor.clearColor;
    contentTextView.text = content;
    contentTextView.font = [UIFont systemFontOfSize:ZoomSize(14)];
    contentTextView.textColor = UIColor.whiteColor;
    contentTextView.textAlignment = NSTextAlignmentCenter;
    contentTextView.editable = NO;
    contentTextView.scrollEnabled = YES;
    
    
}

- (void) setData {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backAction) name:NeedRefleshProfilePage object:nil]; //编辑用户设置
}

#pragma function
- (UIButton *) createFunctionBtWithFrame:(CGRect) frame titleString:(NSString *) titleString tipString:(NSString *) tipString btnTag:(int) btnTag  {
    
    UIButton *funcBt = [[UIButton alloc] initWithFrame:frame];
    [funcBt setTitle:titleString forState:UIControlStateNormal];
    [funcBt setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    funcBt.titleLabel.font = [UIFont systemFontOfSize:ZoomSize(18)];
    funcBt.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    funcBt.titleEdgeInsets = UIEdgeInsetsMake(0, ZoomSize(13), 0, 0);
    
    funcBt.layer.cornerRadius = ZoomSize(6);
    funcBt.layer.masksToBounds = YES;
    funcBt.layer.borderColor = UIColor.purpleColor.CGColor;
    funcBt.layer.borderWidth = ZoomSize(2);
    
    funcBt.tag = btnTag;
    [funcBt addTarget:self action:@selector(switchFunctionAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UILabel *titleLb = [[UILabel alloc] initWithFrame:CGRectMake( SCREEN_Width - ZoomSize(130), ZoomSize(0), ZoomSize(80), ZoomSize(50))];
    [funcBt addSubview:titleLb];
    titleLb.textColor = WhiteColor;
    titleLb.textAlignment = NSTextAlignmentRight;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(18) weight:UIFontWeightSemibold];
    titleLb.text = tipString;
    
    
    return funcBt;
}


- (void)switchFunctionAction:(UIButton *) sender {
    
    
    switch (sender.tag) {
        case 110: {
            [[VDPurchaseTool shareInstance] getProductInfo:vip1week];
            break;
        }
        case 120: {
            [[VDPurchaseTool shareInstance] getProductInfo:vip1month];
            break;
        }
        case 130: {
            [[VDPurchaseTool shareInstance] getProductInfo:vip1year];
            break;
        }
            
        default: break;
    }
}



- (void) backAction {
    NSLog(@"settingAction");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) restoreAction {
    NSLog(@"restoreAction");
    [[VDPurchaseTool shareInstance] restorePurchase:YES];
}

- (void) termsAction {
    NSLog(@"termsAction");
    [[VDPurchaseTool shareInstance] restorePurchase:YES];
    
        VDWebVC *ActivityVC = [[VDWebVC alloc] init];
        ActivityVC.url = @"https://sites.google.com/view/catoapp/terms-of-use";
        [[VDDataTool getCurrentVC] presentViewController:ActivityVC animated:YES completion:nil];
        
        
}

- (void) privacyAction {
    VDWebVC *ActivityVC = [[VDWebVC alloc] init];
    ActivityVC.url = @"https://sites.google.com/view/catoapp/privacy-policy";
    [[VDDataTool getCurrentVC] presentViewController:ActivityVC animated:YES completion:nil];
}


@end
