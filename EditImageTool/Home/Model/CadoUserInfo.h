//
//  CadoUserInfo.h
//  EditImageTool
//
//  Created by steve on 2020/8/24.
//  Copyright © 2020 India. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CadoUserInfo : NSObject

@property (nonatomic, strong) NSString *vipOverTimeString;

+ (instancetype) shareInstant;

- (void) loadUserinfoFromSadeBox;
- (void) saveUserInfoToSadeBox;
- (BOOL) checkUserVip;

@end

NS_ASSUME_NONNULL_END
