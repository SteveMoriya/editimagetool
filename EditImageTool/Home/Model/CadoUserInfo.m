//
//  CadoUserInfo.m
//  EditImageTool
//
//  Created by steve on 2020/8/24.
//  Copyright © 2020 India. All rights reserved.
//

#import "CadoUserInfo.h"

static id _shareInstance = nil;

@interface CadoUserInfo() {
    
}

@end

@implementation CadoUserInfo

+ (instancetype)shareInstant {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareInstance = [[CadoUserInfo alloc] init];
    });
    return _shareInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self loadUserinfoFromSadeBox];
        
    }
    return self;
}

- (void)loadUserinfoFromSadeBox {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    self.vipOverTimeString = [userDefaults objectForKey:UserVipOverTime];
}

- (void)saveUserInfoToSadeBox {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.vipOverTimeString forKey:UserVipOverTime];
    [userDefaults synchronize];
}

- (BOOL) checkUserVip {
    BOOL isVip = false;
    if (self.vipOverTimeString.length > 0) {
        NSTimeInterval nowInterval = [[NSDate date] timeIntervalSince1970];
        double vipInterval = [self.vipOverTimeString doubleValue];
        
        if (vipInterval > nowInterval) {
            isVip = true;
        }
    }
    
    return isVip;
    
}


@end
