//
//  ViewController.m
//  EditImageTool
//
//  Created by steve on 2020/8/11.
//  Copyright © 2020 India. All rights reserved.
//

#import "ViewController.h"
#import "VDChoosePhotoTool.h"
#import <Photos/Photos.h>
#import "AFNetworking.h"

@interface ViewController () {
    UIImageView *getImageView ;
    NSData *getImageData;
//    UIImageView *exchangeImageView ;
    
    NSString *tokenString;
    AFHTTPSessionManager *manager;
}

@property (nonatomic, strong) VDChoosePhotoTool *photoManager;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setPage];
    [self setData];
}

- (void) setPage {
    
    self.view.backgroundColor = UIColor.whiteColor;
    
    UIButton * chooseBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(100), ZoomSize(200), ZoomSize(30))];
    [self.view addSubview:chooseBt];
    chooseBt.backgroundColor = UIColor.blackColor;
    [chooseBt setTitle:@"choose A photo" forState:UIControlStateNormal];
    [chooseBt addTarget:self action:@selector(chooseAction) forControlEvents:UIControlEventTouchUpInside];
    
    getImageView = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(150), ZoomSize(200), ZoomSize(200))];
    getImageView.backgroundColor = UIColor.blackColor;
    getImageView.contentMode = UIViewContentModeScaleAspectFill;
    getImageView.layer.masksToBounds = YES;
    [self.view addSubview:getImageView];
    
    NSArray *titleArray = [NSArray arrayWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8", nil];
    for (int i = 0; i < titleArray.count; i++) {
        
        UIButton * exchangeBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(20) +  ZoomSize(30) * i , ZoomSize(400), ZoomSize(20), ZoomSize(30))];
        exchangeBt.tag = 100 + i;
        [self.view addSubview:exchangeBt];
        exchangeBt.backgroundColor = UIColor.blackColor;
        [exchangeBt setTitle:titleArray[i] forState:UIControlStateNormal];
        [exchangeBt addTarget:self action:@selector(exchangeAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
//    exchangeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(480), ZoomSize(200), ZoomSize(200))];
//    exchangeImageView.backgroundColor = UIColor.blackColor;
//    exchangeImageView.contentMode = UIViewContentModeScaleAspectFill;
//    exchangeImageView.layer.masksToBounds = YES;
//    [self.view addSubview:exchangeImageView];
    
    UIButton * saveBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(700), ZoomSize(200), ZoomSize(30))];
    [self.view addSubview:saveBt];
    saveBt.backgroundColor = UIColor.blackColor;
    [saveBt setTitle:@"save" forState:UIControlStateNormal];
    [saveBt addTarget:self action:@selector(saveData) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton * shareBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(20), ZoomSize(750), ZoomSize(200), ZoomSize(30))];
    [self.view addSubview:shareBt];
    shareBt.backgroundColor = UIColor.blackColor;
    [shareBt setTitle:@"share" forState:UIControlStateNormal];
    [shareBt addTarget:self action:@selector(shareData) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void) setData {
    _photoManager = [[VDChoosePhotoTool alloc] init];
    
    manager = [[AFHTTPSessionManager alloc] init];
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //request 格式
    manager.responseSerializer = [AFJSONResponseSerializer serializer]; //response 格式
    manager.requestSerializer.timeoutInterval = 30.f;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html",nil];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
//    获取token
    NSDictionary *dic = [[NSDictionary alloc] init];
    [manager POST:TokenUrl parameters:dic headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dataDic = responseObject;
        self->tokenString= dataDic[@"access_token"];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    }];
    
}

- (void) saveData {
    NSLog(@"saveData");
    
    [[PHPhotoLibrary sharedPhotoLibrary]performChanges:^{
        [PHAssetChangeRequest creationRequestForAssetFromImage:self->getImageView.image];
        
    } completionHandler:^(BOOL success, NSError * _Nullable error) {
        if (error) {
            [VDDataTool showHUDWithString:@"Save failed!"];
        } else {
            [VDDataTool showHUDWithString:@"Save success!"];
        }
    }];
    
}

- (void) shareData {
    NSLog(@"shareData");
    
    NSString *shareTitle = @"";
    UIImage *shareImage = getImageView.image;
    NSURL *shareUrl = [NSURL URLWithString:@"https://apps.apple.com/us/app/id1519402666"];
    NSArray *activityItemsArray = @[shareTitle, shareImage, shareUrl]; // 分享内容
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItemsArray applicationActivities:nil];
    activityVC.modalInPopover = YES;
    
    activityVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
    };
    
    [self.navigationController presentViewController:activityVC animated:YES completion:nil];
    
}

#pragma function

- (void) chooseAction  {
    NSLog(@"choose Action");
    
    [_photoManager startSelectPhotoWithImageName:@"Choose Photo"];
    __weak __typeof (self) weakSelf = self;
    
    _photoManager.successHandle = ^(VDChoosePhotoTool * _Nonnull manager, UIImage * _Nonnull originalImage, UIImage * _Nonnull resizeImage) {
        
        __strong __typeof(self) strongSelf = weakSelf;
        strongSelf->getImageView.image = originalImage;
        strongSelf->getImageData = UIImageJPEGRepresentation([VDDataTool compressSizeImage:resizeImage], 0.25);
        
    };
    
}

- (void) exchangeAction :(UIButton *)sender {
    
    //    image    true    string    -    需要处理的图片base64编码后大小不超过4M，最短边至少64px，最长边最大4096px，长宽比3：1以内。注意：图片的base64编码是不包含图片头的，如（data:image/jpg;base64,）
    //    type    false    string    -    anime或者anime_mask。前者生成二次元动漫图，后者生成戴口罩的二次元动漫人像
    //    mask_id    false    int    1-8    在type参数填入anime_mask时生效，1～8之间的整数，用于指定所使用的口罩的编码。type参数没有填入anime_mask，或mask_id 为空时，生成不戴口罩的二次元动漫图。
    
    [VDDataTool startShowHUD];
     NSString *imageStr = [getImageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSDictionary *dic = [[NSDictionary alloc] init];
    int senderTag = (int)sender.tag - 100;
    if (senderTag > 0) {
        dic = @{@"image":imageStr, @"type":@"anime_mask", @"mask_id":[NSNumber numberWithInt:senderTag] };
    } else {
        dic = @{@"image":imageStr};
    }
   
    [manager POST:[NSString stringWithFormat:@"%@%@", ImgUrl, tokenString] parameters:dic headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [VDDataTool dismissHUD];
        
        NSDictionary *dataDic = responseObject;
        NSString *getImgStr = dataDic[@"image"];
        NSData * imageData =[[NSData alloc] initWithBase64EncodedString:getImgStr options:NSDataBase64DecodingIgnoreUnknownCharacters];
        UIImage *image = [UIImage imageWithData: imageData];
        
        self->getImageView.image = image;
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [VDDataTool dismissHUD];
        
    }];
    
}


@end
