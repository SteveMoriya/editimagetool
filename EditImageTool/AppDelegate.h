//
//  AppDelegate.h
//  EditImageTool
//
//  Created by steve on 2020/8/11.
//  Copyright © 2020 India. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

