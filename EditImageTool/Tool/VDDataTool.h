//
//  VDDataTool.h
//  Vido
//
//  Created by steve on 2020/8/10.
//  Copyright © 2020 VD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM (NSInteger ,profileVisitType ) {
    VisitStangerProfile = 0,
    VisitSelfProfile = 1,
    VisitFriendProfile = 2,
    VisitDiscoverProfile = 3
};

@interface VDDataTool : NSObject

+ (UIViewController *) getCurrentVC;
+ (void) showHomePage;
+ (void) showBuyVipPage;
+ (void) startShowHUD;
+ (void) dismissHUD;
+ (void) showHUDWithString:(NSString *)string;
    
+ (UIImage *) compressSizeImage:(UIImage *)image;

@end

NS_ASSUME_NONNULL_END
