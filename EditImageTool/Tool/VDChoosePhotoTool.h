//
//  VDChoosePhotoTool.h
//  Vido
//
//  Created by steve on 2020/8/10.
//  Copyright © 2020 VD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JPImageresizerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface VDChoosePhotoTool : NSObject<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property(nonatomic, strong) void (^successHandle)(VDChoosePhotoTool *manager, UIImage *originalImage, UIImage *resizeImage);
@property(nonatomic, strong) void (^errorHandle)(NSString *error);
@property(nonatomic, strong) void (^clickHandle)(void);

@property(nonatomic, assign) UIStatusBarStyle statusBarStyle;
@property(nonatomic, strong) JPImageresizerConfigure *configure;
@property(nonatomic, weak) JPImageresizerView *imageresizerView;
@property(nonatomic, assign) JPImageresizerFrameType frameType;

- (void)startSelectPhotoWithImageName:(NSString *)imageName;
- (void)openPhoto;
- (void)chooseLibrary;

@end

NS_ASSUME_NONNULL_END
