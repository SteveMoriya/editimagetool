//
//  VDWebVC.h
//  Vido
//
//  Created by steve on 2020/8/10.
//  Copyright © 2020 VD. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VDWebVC : UIViewController

@property (nonatomic, strong) NSString *url;

@end

NS_ASSUME_NONNULL_END
