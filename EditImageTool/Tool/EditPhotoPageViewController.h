
//
//  VDChoosePhotoTool.m
//  Vido
//
//  Created by steve on 2020/8/10.
//  Copyright © 2020 VD. All rights reserved.
//

#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface EditPhotoPageViewController : UIViewController

@property (nonatomic, strong) UIImage *getImage;
@property (nonatomic, copy) CallBack callbackImg;

@end


NS_ASSUME_NONNULL_END
