//
//  VDPurchaseTool.m
//  EditImageTool
//
//  Created by steve on 2020/8/24.
//  Copyright © 2020 India. All rights reserved.
//

#import "VDPurchaseTool.h"
#import <StoreKit/StoreKit.h>

static id _shareInstance = nil;

@interface VDPurchaseTool()<SKPaymentTransactionObserver,SKProductsRequestDelegate> {
    NSMutableArray *payTransaction; //需要处理的账单
    BOOL needShowHub;
}

@end

@implementation VDPurchaseTool

+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shareInstance = [[VDPurchaseTool alloc] init];
    });
    return _shareInstance;
}

- (instancetype) init {
    self = [super init];
    if (self) {
       [self setupPurchaseTool];
    }
    return self;
}

- (void) setupPurchaseTool {
    payTransaction = [NSMutableArray array];
//    hubView = [[CDHUBView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, SCREEN_Height)];
    needShowHub = YES;
}

- (void) addTranObserver {
    [[SKPaymentQueue defaultQueue] addTransactionObserver:self]; //添加支付结果监听a
}

- (BOOL)paymentQueue:(SKPaymentQueue *)queue shouldAddStorePayment:(SKPayment *)payment forProduct:(SKProduct *)product {
    return YES;
}

- (void)getProductInfo:(NSString *)productIdentifier{
    
    if (productIdentifier.length > 0){
         [VDDataTool startShowHUD];
    
        NSArray * product = [[NSArray alloc] initWithObjects:productIdentifier, nil];
        NSSet *set = [NSSet setWithArray:product];
        SKProductsRequest * request = [[SKProductsRequest alloc] initWithProductIdentifiers:set];
        request.delegate = self;
        [request start];//开始请求
    }
}

- (void)restorePurchase :(BOOL) needHub {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    needShowHub = needHub;
    
    if (needShowHub) {
        [VDDataTool startShowHUD];
    }
}

#pragma product

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response{
    NSLog(@"didReceiveResponse");
    NSArray *product = response.products;
    if([product count] == 0){
       [VDDataTool dismissHUD];
        return;
    }
    
    SKProduct *p = nil;
    for (SKProduct *pro in product) {
        p = pro;
        SKPayment *payment = [SKPayment paymentWithProduct:p];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        break;
    }
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error{
    NSLog(@"didFailWithError");
    [VDDataTool dismissHUD];
}

- (void)requestDidFinish:(SKRequest *)request{
    NSLog(@"requestDidFinish");
}

// 监听购买结果
- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray<SKPaymentTransaction *> *)transactions {
    
    NSLog(@"updatedTransactions");
    [payTransaction removeAllObjects]; //清除数据
    
    for (SKPaymentTransaction *tran in transactions) {
        
        switch (tran.transactionState) {
            case SKPaymentTransactionStatePurchased:  { //交易完成
                [self addTranToCheck:tran];
                [[SKPaymentQueue defaultQueue]finishTransaction:tran];
            }
                break;
            case SKPaymentTransactionStatePurchasing: {
            }
                break;
            case SKPaymentTransactionStateRestored:  { //购买过
                
                [self addTranToCheck:tran];
                [[SKPaymentQueue defaultQueue]finishTransaction:tran];
                
                if (needShowHub) {
                    [VDDataTool showHUDWithString:@"Restored Success"];
                }
            }
                break;
            case SKPaymentTransactionStateFailed:  { //交易失败
                [[SKPaymentQueue defaultQueue]finishTransaction:tran];
            }
                break;
            default: {
            }
                break;
        }
    }
    
    if (payTransaction.count > 0) {
        
        
        NSArray *orderedDateArray = [self->payTransaction sortedArrayUsingComparator:^NSComparisonResult(SKPaymentTransaction *tran1, SKPaymentTransaction *tran2) {
            return [tran1.transactionDate compare: tran2.transactionDate];
        }];
        
        SKPaymentTransaction *lastTran = orderedDateArray.lastObject;
        float overTime = 0;
        if ([lastTran.payment.productIdentifier isEqualToString:vip1week]) {
            overTime = 7 * 24 * 60 * 60;
        } else if ([lastTran.payment.productIdentifier isEqualToString:vip1month]) {
            overTime = 30 * 24 * 60 * 60;
        } else if ([lastTran.payment.productIdentifier isEqualToString:vip1year]) {
            overTime = 365 * 24 * 60 * 60;
        }
        
        NSTimeInterval a = [lastTran.transactionDate timeIntervalSince1970];
        NSString *timeString = [NSString stringWithFormat:@"%0.f", a + overTime];
        
        [CadoUserInfo shareInstant].vipOverTimeString = timeString;
        [[CadoUserInfo shareInstant] saveUserInfoToSadeBox];
        
        NSLog(@"%@ %@", lastTran.transactionDate, timeString);
        [self finishTransactionAction];
    }
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray<SKPaymentTransaction *> *)transactions {
    NSLog(@"removedTransactions");
    [VDDataTool dismissHUD];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error  {
    NSLog(@"restoreCompleted");
    [VDDataTool dismissHUD];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    [VDDataTool dismissHUD];
}

- (void) addTranToCheck: (SKPaymentTransaction *)tran {
    BOOL canAdd = YES;
    for (SKPaymentTransaction *hasTran in self->payTransaction) {
        if ([hasTran.transactionIdentifier isEqualToString:tran.transactionIdentifier]) {
            canAdd = NO;
            return;
        }
    }
    
    if (canAdd) {
        [payTransaction addObject:tran];
    }
}

- (void) finishTransactionAction {
    [VDDataTool dismissHUD];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:NeedRefleshProfilePage object:nil userInfo:nil];
    });
    
    
    for (SKPaymentTransaction *tran in self->payTransaction) {

        if (tran.transactionState == SKPaymentTransactionStatePurchased) {
            [[SKPaymentQueue defaultQueue]finishTransaction:tran];
        } else if (tran.transactionState == SKPaymentTransactionStateRestored) {
            [[SKPaymentQueue defaultQueue]finishTransaction:tran];
        }
    }
}


@end
