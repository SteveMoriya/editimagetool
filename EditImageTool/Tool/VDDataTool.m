//
//  VDDataTool.m
//  Vido
//
//  Created by steve on 2020/8/10.
//  Copyright © 2020 VD. All rights reserved.
//

#import "VDDataTool.h"
#import "SVProgressHUD.h"
#import "HomeViewController.h"
#import "UserBuyVipViewController.h"

@implementation VDDataTool

+ (void) showHomePage {
    HomeViewController *vc = [[HomeViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.navigationBar.hidden = YES;
    [UIApplication sharedApplication].delegate.window.rootViewController = nav;
}

+ (void) showBuyVipPage {
    UserBuyVipViewController *vc = [[UserBuyVipViewController alloc] init];
    vc.modalPresentationStyle = 0;
    [[VDDataTool getCurrentVC].navigationController presentViewController:vc animated:YES completion:nil];
}

+ (UIViewController *)getCurrentVC {
    UIViewController *vc = [UIApplication sharedApplication].keyWindow.rootViewController;
    UIViewController *currentShowingVC = [self findCurrentShowingViewControllerFrom:vc];
    return currentShowingVC;
}

+ (UIViewController *)findCurrentShowingViewControllerFrom:(UIViewController *)vc {
    UIViewController *currentShowingVC; //方法1：递归方法 Recursive method
    if ([vc presentedViewController]) { //注要优先判断vc是否有弹出其他视图，如有则当前显示的视图肯定是在那上面
        UIViewController *nextRootVC = [vc presentedViewController]; // 当前视图是被presented出来的
        currentShowingVC = [self findCurrentShowingViewControllerFrom:nextRootVC];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        UIViewController *nextRootVC = [(UITabBarController *)vc selectedViewController];  // 根视图为UITabBarController
        currentShowingVC = [self findCurrentShowingViewControllerFrom:nextRootVC];
    } else if ([vc isKindOfClass:[UINavigationController class]]){
        UIViewController *nextRootVC = [(UINavigationController *)vc visibleViewController]; // 根视图为UINavigationController
        currentShowingVC = [self findCurrentShowingViewControllerFrom:nextRootVC];
    } else {
        currentShowingVC = vc; // 根视图为非导航类
    }
    return currentShowingVC;
}

+ (void) startShowHUD {
    [SVProgressHUD dismissWithDelay:60.0f];
    [SVProgressHUD show];
}

+ (void) dismissHUD {
    [SVProgressHUD dismiss];
}

+ (void) showHUDWithString:(NSString *)string {
    [SVProgressHUD showSuccessWithStatus:string];
    [SVProgressHUD dismissWithDelay:2.0f];
}

+ (UIImage *)compressSizeImage:(UIImage *)image {
    CGSize size = image.size;
    UIImage *resultImage = image;
    if (size.width >= size.height) {
        if (size.height > 1080) {
            int newWidth = (size.width/size.height) * 1080;
            int newHeight = 1080;
            UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
            [resultImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
            resultImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return resultImage;
        } else {
            return image;
        }
    } else {
        if (size.width > 1080) {
            int newWidth = 1080;
            int newHeight = (size.height / size.width) * 1080;
            UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
            [resultImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
            resultImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return resultImage;
        } else {
            return image;
        }
    }
    return image;
}



@end
