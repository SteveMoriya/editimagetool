//
//  VDChoosePhotoTool.m
//  Vido
//
//  Created by steve on 2020/8/10.
//  Copyright © 2020 VD. All rights reserved.
//

#import "VDChoosePhotoTool.h"
#import "EditPhotoPageViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface VDChoosePhotoTool() {
    UIImagePickerController *ipVC;
}

@end

@implementation VDChoosePhotoTool

- (void)startSelectPhotoWithImageName:(NSString *)imageName{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle: UIAlertControllerStyleActionSheet];
    [alertController addAction: [UIAlertAction actionWithTitle: @"Take a Photo" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self selectPhotoWithType:0];
    }]];
    [alertController addAction: [UIAlertAction actionWithTitle: @"Choose from Library" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self selectPhotoWithType:1];
    }]];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:cancelAction];
    
    [[VDDataTool getCurrentVC] presentViewController:alertController animated:YES completion:nil];
}

- (void)openPhoto {
    [self selectPhotoWithType:0];
}

- (void)chooseLibrary {
    [self selectPhotoWithType:1];
}

#pragma mark function
-(void)selectPhotoWithType:(int)type {
    ipVC = [[UIImagePickerController alloc] init];
    ipVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    ipVC.modalPresentationStyle = UIModalPresentationOverFullScreen;
    ipVC.preferredContentSize = CGSizeMake(SCREEN_Width,  SCREEN_Width);
    ipVC.delegate = self;
    if (type == 0) {
        BOOL isCamera = [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
        if (!isCamera) {
            return ;
        }else{
            ipVC.sourceType = UIImagePickerControllerSourceTypeCamera;
            ipVC.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
    }else{
        ipVC.sourceType = UIImagePickerControllerCameraCaptureModePhoto;
    }
    [self presentAction:ipVC];
}

- (void)presentAction:(id)vc{
    [[VDDataTool getCurrentVC] presentViewController:vc animated:YES completion:nil];
}

#pragma mark
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSURL* url;
    UIImage * image;
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:@"public.image"]) {
        if (@available(iOS 11.0, *)) {
            url = [info objectForKey:UIImagePickerControllerImageURL];
            if (url != nil) {
                NSData *imageData = [NSData dataWithContentsOfURL:url];
                image = [UIImage imageWithData:imageData];
            } else {
                UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
                if (originalImage != nil) {
                    image = originalImage;
                }
                UIImage *editedImage = [info objectForKey:UIImagePickerControllerEditedImage];
                if (editedImage != nil) {
                    image = editedImage;
                }
            }
        } else {
            UIImage *originalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
            if (originalImage != nil) {
                image = originalImage;
            }
            UIImage *editedImage = [info objectForKey:UIImagePickerControllerEditedImage];
            if (editedImage != nil) {
                image = editedImage;
            }
        }
    }
    if (image == nil) {
//        [VDDataTool showHUDWithString:@"The picture is not available, please change it"];
        return;
    }
    
    EditPhotoPageViewController *VC = [[EditPhotoPageViewController alloc] init];
    VC.getImage = image;
    [ipVC pushViewController:VC animated:YES];
    VC.callbackImg = ^(id  _Nullable obj) {
        UIImage *getImage = obj;
        if (getImage == nil) {
            getImage = image;
        }
        if (self->_successHandle) {
            self->_successHandle(self, image, getImage);
        }
    };
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [[VDDataTool getCurrentVC] dismissViewControllerAnimated:YES completion:nil];
}


@end
