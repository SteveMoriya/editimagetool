//
//  VDWebVC.m
//  Vido
//
//  Created by steve on 2020/8/10.
//  Copyright © 2020 VD. All rights reserved.
//

#import "VDWebVC.h"
#import <WebKit/WebKit.h>

@interface VDWebVC ()<WKNavigationDelegate> {
    UILabel *titleLb;
}

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) UIProgressView *progressView;

@end

@implementation VDWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [self setupNav];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
    [self.webView removeObserver:self forKeyPath:@"title"];
}

- (void)setupNav {
    self.navigationController.navigationBar.hidden = YES;
}

- (void)setUI {
    self.view.backgroundColor = UIColor.blackColor;
    
    UIView *navBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, NavigationHeight)];;
//    navBgView.backgroundColor = BGDarkBlueColor;
    [self.view addSubview:navBgView];
    
    UIButton *backBt = [[UIButton alloc] initWithFrame:CGRectMake(ZoomSize(10), StatusBarHeight, ZoomSize(30), ZoomSize(30))];
    [navBgView addSubview:backBt];
    [backBt setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    [backBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    
    titleLb = [[UILabel alloc] initWithFrame:CGRectMake( ZoomSize(60), StatusBarHeight, ZoomSize(240), ZoomSize(30))];
    [navBgView addSubview:titleLb];
    titleLb.textColor = WhiteColor;
    titleLb.adjustsFontSizeToFitWidth = YES;
    titleLb.textAlignment = NSTextAlignmentLeft;
    titleLb.font = [UIFont systemFontOfSize:ZoomSize(24) weight:UIFontWeightSemibold];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    [self.view addSubview:self.webView];
    [self.webView addSubview:self.progressView];
}

#pragma mark - funtion
- (void)backAction {
    [_webView removeObserver:self forKeyPath:@"estimatedProgress"];
    [_webView removeObserver:self forKeyPath:@"title"];
    if (self.navigationController != nil) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - lazy methods
- (WKWebView *)webView {
    if (_webView == nil) {
        _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, NavigationHeight , SCREEN_Width , SCREEN_Height - NavigationHeight )];
        _webView.navigationDelegate = self;
        _webView.layer.masksToBounds = YES;
    }
    [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [_webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
    return _webView;
}

- (UIProgressView *)progressView {
    if (_progressView == nil) {
        _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(0, 0, NavigationHeight, 2)];
    }
    _progressView.progressViewStyle = UIProgressViewStyleBar;
    return _progressView;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        CGFloat progress = [change[NSKeyValueChangeNewKey] floatValue];
        [self.progressView setProgress:progress animated:YES];
        if(progress == 1.0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.progressView setProgress:0.0 animated:NO];
            });
        }
    }
    else if ([keyPath isEqualToString:@"title"]) {
        titleLb.text = self.webView.title;
    }
}


@end
