//
//  VDPurchaseTool.h
//  EditImageTool
//
//  Created by steve on 2020/8/24.
//  Copyright © 2020 India. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface VDPurchaseTool : NSObject

+ (instancetype)shareInstance;

- (void) addTranObserver;
- (void) getProductInfo:(NSString *)productIdentifier;
- (void) restorePurchase :(BOOL) needHub;

@end

NS_ASSUME_NONNULL_END
