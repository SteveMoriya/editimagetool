//
//  AppDelegate.m
//  EditImageTool
//
//  Created by steve on 2020/8/11.
//  Copyright © 2020 India. All rights reserved.
//

#import "AppDelegate.h"
#import "VDPurchaseTool.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[VDPurchaseTool shareInstance] addTranObserver]; //添加支付监听
    [VDDataTool showHomePage];
    
    // Override point for customization after application launch.
    return YES;
}



@end
